# Minecraft Electrical Age - Core package

This package is the central part of the project. This is where datapoints are created and stored.

## Useful information

In Minecraft Electrical Age, we work with mainly two types of data:

* Float datapoints
* Binary (or boolean) datapoints

These two types of datapoints store a value (which is a float or a boolean) coming from the Minecraft
world. They are stored in a hashmap and are retrieved using an identification label.

When their value is changing (see Javadoc for a more detailed answer), they inform the webpage, the database
and the minecraft world (via the field, if necessary).

Note: You can download [the following file](https://cyberlearn.hes-so.ch/pluginfile.php/3575151/mod_label/intro/Modbus%20specification%20sheet.pdf?time=1620654911485)
in order to know datapoints label, value, Modbus address, state (input or output), range and offset.

## Authors and contributors

* **Filip Simeonovic**
* **Vincent Savioz**
