package ch.hevs.isi.core;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * This abstract class is used to create a Datapoint
 * and to initialize the values for the tests with the main
 *
 * @author Filip
 * @version 3.1
 */
public abstract class DataPoint {
    private static HashMap<String, DataPoint> dataPointMap = new HashMap<>();
    private String label;
    private boolean isOutput;

    /**
     *  Constructor used to initialize a Datapoint
     *
     * @param label : Parameter used to set the name of the measured element
     * @param isOutput : Parameter used to set a datapoint in output mode or not
     */
    protected DataPoint(String label, boolean isOutput){
        this.label = label;
        this.isOutput = isOutput;

        dataPointMap.put(label, this);
    }

    /**
     *  Method used to get the Datapoint according to the label
     *
     * @param label : Label of the searched Datapoint
     * @return a Datapoint
     */
    public static DataPoint getDataPointFromLabel(String label){

        return dataPointMap.get(label);
    }

    /**
     * Method used to get the label of the Datapoint
     *
     * @return the label of the Datapoint
     */
    public String getLabel(){

        return label;
    }

    /**
     * Method used to know if the Datapoint is an Output or not
     *
     * @return the output of the Datapoint
     */
    public boolean isOutput(){

        return isOutput;
    }

    //Abstract method defined in child classes
    public abstract String toString();
    public abstract boolean fromString(String s) throws IOException;

    //Main used for the tests
    public static void main(String[] args) throws IOException, InterruptedException {

        //Declaration of some Datapoints for the tests
        BinaryDataPoint bd = new BinaryDataPoint("BATT_P_FLOAT", false);
        FloatDataPoint fd1 = new FloatDataPoint("BATT_CHRG_FLOAT", true);
        FloatDataPoint fd2 = new FloatDataPoint("BATT_P_FLOAT", true);
        FloatDataPoint fd3 = new FloatDataPoint("REMOTE_FACTORY_SP", true);

        float index1 = 0;
        float index2 = 0;

        while(true) {
            fd1.setValue(index1);
            fd2.setValue(index2);
            index1 += 0.01;
            index2 += 1;
            //bd.setValue(true);

            TimeUnit.SECONDS.sleep(2);
        }
    }
}