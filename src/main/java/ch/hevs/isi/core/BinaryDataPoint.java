package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.io.IOException;

/**
 * This class extends DataPoint to create a DataPoint
 * with a boolean value and a specific label
 *
 * @author Filip
 * @version 2.0
 */
public class BinaryDataPoint extends DataPoint{
    private boolean value;

    /**
     * The constructor call the constructor of the super Class
     *
     * @param label : Parameter used to set the name of the measured element
     * @param isOutput : Parameter used to set a datapoint in output mode or not
     */
    public BinaryDataPoint(String label, boolean isOutput){
        super(label, isOutput);

        value = false;
    }

    /**
     * Method used to transform a boolean in a String
     * Overwritten method from the super Class
     *
     * @return a String of the variable value
     */
    @Override
    public String toString() {
        return String.valueOf(value);
    }

    /**
     * Call the method setValue to set the variable value according to the String s
     * Overwritten method from the super Class
     *
     * @param s : Label that contains true or false
     * @return a boolean
     */
    @Override
    public boolean fromString(String s){
        try {
                if(s.equals("true")){
                    setValue(true);
                    return true;
                }
                else if(s.equals("false")) {
                    setValue(false);
                    return true;
                }
                return false;
            }catch (IOException e){
                e.printStackTrace();
                return false;
            }
    }

    /**
     * Method used to set the variable value and notify the
     * Database and WebConnector
     *
     * @param value : used to set the Variable value
     * @throws IOException
     */
    public synchronized void setValue(boolean value) throws IOException {
        if(this.value != value){
            this.value = value;
            DatabaseConnector.getInstance().onNewValue(this);
            WebConnector.getInstance().onNewValue(this);

            if(isOutput())
                FieldConnector.getInstance().onNewValue(this);
        }
    }

    /**
     * Method used to get the value of the Variable value
     *
     * @return a boolean
     */
    public boolean getValue(){
        return value;
    }
}
