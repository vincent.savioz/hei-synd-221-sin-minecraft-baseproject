package ch.hevs.isi.core;

import java.io.IOException;

/**
 * This interface is used to fill the connector
 * when a value has changed
 *
 * @author Filip
 * @version 1.0
 */
public interface DataPointListener {
    /**
     * Is called to inform classes that a float datapoint
     * has been modified.
     * @param fdp
     */
    void onNewValue(FloatDataPoint fdp) throws IOException;

    /**
     * Is called to inform classes that a binary datapoint
     * has been modified.
     * @param bdp
     */
    void onNewValue(BinaryDataPoint bdp) throws IOException;
}
