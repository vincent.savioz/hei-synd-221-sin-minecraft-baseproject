# Minecraft Electrical Age - SmartController package

The SmartController package is the brain of the project. This is where we take value coming from
Minecraft and generate output accordingly.

## Useful information

The *SmartController* class is a singleton containing a control method used to regulate the Minecraft
world. At its first instanciation, it creates a poll task using a timer, which is called every
5 seconds. 

For regulation details, please see the *doControl* method of the *SmartController* class.

## Authors and contributors

* **Filip Simeonovic**
* **Vincent Savioz**
