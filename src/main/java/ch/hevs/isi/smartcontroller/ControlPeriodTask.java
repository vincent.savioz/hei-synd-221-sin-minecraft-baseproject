package ch.hevs.isi.smartcontroller;

import java.util.TimerTask;

/**
 * This class in instantiated on every SmartController
 * timer timeout. It calls the control method of the
 * SmartController class.
 */
public class ControlPeriodTask extends TimerTask {

    /**
     * Constructor. Call the constructor of the superclass.
     */
    public ControlPeriodTask() {
        super();
    }

    /**
     * Is called by the thread. Calls the doControl method
     * of the SmartController.
     */
    @Override
    public void run() {
        SmartController sc = SmartController.getInstance();
        sc.doControl();
    }
}
