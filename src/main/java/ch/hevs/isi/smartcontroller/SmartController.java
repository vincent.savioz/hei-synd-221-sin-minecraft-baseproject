package ch.hevs.isi.smartcontroller;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;
import java.util.Timer;

/**
 * This class implement the brain of the application.
 * It compute different data coming from the field
 * and adapt outputs.
 *
 * @version 2.0
 * @author Vincent Savioz
 */
public class SmartController {
    private int pollPeriod = 5000;  //Compute every 5 seconds

    private static SmartController sc;
    private static Timer controlPeriod;

    /**
     * Empty private constructor
     */
    private SmartController(){

    }

    /**
     * Get the singleton instance of the class
     * @return  The only instance
     */
    public static SmartController getInstance(){
        if(sc == null){
            sc = new SmartController();

            //On first initialization, create the poll task
            controlPeriod = new Timer();
            controlPeriod.scheduleAtFixedRate(new ControlPeriodTask(), 2500, sc.pollPeriod);
        }

        return sc;
    }

    /**
     * Compute input and adapt output. This method
     * is called from the poll task.
     */
    public void doControl() {
        System.out.println("\nNew iteration\n");

        //Retrieve interesting datapoint
        FloatDataPoint coalDP = ((FloatDataPoint)DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"));
        FloatDataPoint factoryDP = ((FloatDataPoint)DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"));
        BinaryDataPoint solarDP = ((BinaryDataPoint)DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW"));
        BinaryDataPoint windDP = ((BinaryDataPoint)DataPoint.getDataPointFromLabel("REMOTE_WIND_SW"));

        //Read interesting datapoint value
        float batteryCharge = ((FloatDataPoint)DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT")).getValue();
        float time = ((FloatDataPoint)DataPoint.getDataPointFromLabel("CLOCK_FLOAT")).getValue();
        float score = ((FloatDataPoint)DataPoint.getDataPointFromLabel("SCORE")).getValue();

        //Process data
        try {
            boolean day = false;

            //Determine if it's day or night
            if(time > 0.25f && time < 0.75f)
                day = true;
            else
                day = false;

            System.out.println("Time of the day = " + time);

            System.out.println("BatteryCharge = " + batteryCharge);
            System.out.println("Score = " + score);

            //Solar and wind are always on
            solarDP.setValue(true);
            windDP.setValue(true);

            /*
            DAY: Coal power plant always off, factory adapt depends
            on battery charge.
            NIGHT: Coal power plant on at different rates depending on
            the battery charge. Factory always off, except in case of
            surcharge.
             */
            if(day)
            {
                if(batteryCharge < 0.6){
                    factoryDP.setValue(0.0f);
                    coalDP.setValue(0.0f);
                }
                else if(batteryCharge < 0.9)
                {
                    factoryDP.setValue(0.3f);
                    coalDP.setValue(0.0f);
                }
                else
                {
                    factoryDP.setValue(1.0f);
                    coalDP.setValue(0.0f);
                }
            }
            else
            {
                if(batteryCharge < 0.7){
                    coalDP.setValue(1.0f);
                    factoryDP.setValue(0.0f);
                }
                else if (batteryCharge < 0.9)
                {
                    coalDP.setValue(0.25f);
                    factoryDP.setValue(0.0f);
                } else {
                    coalDP.setValue(0.0f);
                    factoryDP.setValue(1.0f);
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
            System.exit(1);
        }
    }
}
