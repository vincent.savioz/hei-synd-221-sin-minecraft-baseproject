package ch.hevs.isi.field;

import java.nio.ByteBuffer;

/**
 * This class creates a Modbus header (MBAP)
 * @version 1.4
 * @author Vincent
 */
public class MBAP {

    private short transactionID;
    private short protocolID;
    private short length;
    private byte unitID;

    /**
     * Constructor. It creates a MBAP using its parameters
     * @param t     The transaction ID
     * @param p     The protocol ID (Modbus -> 0)
     * @param l     The message length (including the unitID byte)
     * @param u     The unit ID (1 for Minecraft EA)
     */
    public MBAP(short t, short p, short l, byte u){
        transactionID = t;
        protocolID = p;
        length = l;
        unitID = u;
    }

    /**
     * Constructor. It creates a MBAP using a ByteBuffer
     * @param b
     */
    public MBAP(ByteBuffer b){
        transactionID = b.getShort();
        protocolID = b.getShort();
        length = b.getShort();
        unitID = b.get();
    }

    /**
     * Get the MBAP ByteBuffer
     * @return  The ByteBuffer
     */
    public ByteBuffer getByteBuffer(){
        ByteBuffer b = ByteBuffer.allocate(7);
        return b.putShort(transactionID).putShort(protocolID).putShort(length).put(unitID);
    }

    /**
     * Get the transaction ID
     * @return  The transaction ID
     */
    public short getTransactionID(){
        return transactionID;
    }

    /**
     * Get the protocolID
     * @return  The protocol ID
     */
    public short getProtocolID(){
        return protocolID;
    }

    /**
     * Get the message length
     * @return  The message length
     */
    public short getLength() {
        return length;
    }

    /**
     * Get the unit ID
     * @return  The unit ID
     */
    public byte getUnitID(){
        return unitID;
    }
}
