package ch.hevs.isi.field;

import java.nio.ByteBuffer;

/**
 * This class represents a Modbus message (PDU).
 * @version 2.1
 * @author Vincent
 */
public class PDU {

    private ByteBuffer msg;

    /**
     * Store the PDU in a ByteBuffer
     * @param b     The ByteBuffer
     */
    public PDU(ByteBuffer b){
        msg = b;
    }

    /**
     * Get the PDU ByteBuffer
     * @return  The ByteBuffer
     */
    public ByteBuffer getByteBuffer(){
        return msg;
    }
}
