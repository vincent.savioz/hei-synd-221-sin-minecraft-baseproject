package ch.hevs.isi.field;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * This class offers the possibility to read/write
 * boolean and float value of the provided Modbus
 * server, using TCP/IP protocol.
 * This uses a singleton pattern.
 * @verion 3.2
 * @author Vincent Savioz
 */
public class ModbusAccessor extends Socket {

    private final byte RTUAddr = 1;
    private final short protocolID = 0;

    private static ModbusAccessor ma = null;

    private InputStream in;
    private OutputStream out;

    /**
     * Private constructor. Link to the server.
     * @param serverName    The server name
     * @param portNumber    The port number
     * @throws IOException
     */
    private ModbusAccessor(String serverName, int portNumber) throws IOException {
        super(serverName, portNumber);

        in = this.getInputStream();
        out = this.getOutputStream();
    }

    /**
     * Get the only instance of this class
     * @return      The only instance
     * @throws IOException
     */
    public static ModbusAccessor getInstance() throws IOException {
        if(ma == null){
            ma = new ModbusAccessor("localhost", 1502);
        }

        return ma;
    }

    /**
     * Read the value of a coil with the provided address.
     * @param address   The address of the register
     * @return      The boolean value of the register
     * @throws NullPointerException
     * @throws ModbusResponseException
     */
    public boolean readCoil(int address) throws NullPointerException, ModbusResponseException{
        byte fc = 0x01;

        //Generate request byte buffer
        ByteBuffer request = ByteBuffer.allocate(5).put(fc).putShort((short)address).putShort((short)1);

        PDU msg = new PDU(request);
        MBAP header = new MBAP((short)1, protocolID, (short)(msg.getByteBuffer().capacity()+1), RTUAddr);

        Frame toSend = new Frame(header, msg);

        //Set request to the server
        Frame f = Transaction.process(toSend, in, out);

        if(f == null)   //Error receiving response
            throw(new NullPointerException());

        //Decode response
        PDU rMsg = f.getMessage();
        MBAP rHeader = f.getHeader();

        int rFc = (int)(rMsg.getByteBuffer().get() & 0x000000FF);

        //Check received function code for error
        if(rFc == 0x80 + fc || rFc == 0x80)
            throw(new ModbusResponseException(rMsg.getByteBuffer().get()));

        //Read the value
        int rByteCount = (int)(rMsg.getByteBuffer().get() & 0x000000FF);
        int rValue = (int)(rMsg.getByteBuffer().get() & 0x000000FF);

        //Convert the value in a boolean
        if(rValue == 0)
            return false;
        else
            return true;
    }

    /**
     * Write the provided value on the coil (represented by the
     * provided value)
     * @param address   The register address
     * @param value     The desired boolean value
     * @return          True if the write operation was successful.
     * @throws NullPointerException
     * @throws ModbusResponseException
     */
    public boolean writeCoil(int address, boolean value) throws NullPointerException, ModbusResponseException {
        byte fc = 0x05;
        short sVal = 0;

        //Value to write is 1 if true, 0 if false
        if(value)
            sVal = (short)0xFF00;
        else
            sVal = 0x0000;

        //Generate request byte buffer
        ByteBuffer request = ByteBuffer.allocate(5).put(fc).putShort((short)address).putShort(sVal);

        PDU msg = new PDU(request);
        MBAP header = new MBAP((short)1, protocolID, (short)(msg.getByteBuffer().capacity()+1), RTUAddr);

        Frame toSend = new Frame(header, msg);

        //Set request to the server
        Frame f = Transaction.process(toSend, in, out);

        if(f == null)   //Error receiving response
            throw(new NullPointerException());

        //Decode response
        PDU rMsg = f.getMessage();
        MBAP rHeader = f.getHeader();

        int rFc = (int)(rMsg.getByteBuffer().get() & 0x000000FF);

        //Check received function code for error
        if(rFc == 0x80 + fc || rFc == 0x80)
            throw(new ModbusResponseException(rMsg.getByteBuffer().get()));

        //Read the value
        int rByteCount = (int)(rMsg.getByteBuffer().getShort() & 0x0000FFFF);
        int rValue = (int)(rMsg.getByteBuffer().getShort() & 0x0000FFFF);

        //Convert the value in a boolean
        boolean rv = false;
        if(rValue == 0)
            rv =  false;
        else
            rv =  true;

        if(rv == value)
            return true;
        else
            return false;
    }

    /**
     * Read the register at the provided address
     * @param address   The register starting address
     * @return          The read float value
     * @throws ModbusResponseException
     */
    public float readRegister(int address) throws ModbusResponseException {
        byte fc = 0x03;

        //Generate request byte buffer
        ByteBuffer request = ByteBuffer.allocate(5).put(fc).putShort((short)address).putShort((short)2);

        PDU msg = new PDU(request);
        MBAP header = new MBAP((short)1, protocolID, (short)(msg.getByteBuffer().capacity()+1), RTUAddr);

        Frame toSend = new Frame(header, msg);

        //Set request to the server
        Frame f = Transaction.process(toSend, in, out);

        if(f == null)   //Error receiving response
            throw(new NullPointerException());

        //Decode response
        PDU rMsg = f.getMessage();
        MBAP rHeader = f.getHeader();

        int rFc = (int)(rMsg.getByteBuffer().get() & 0x000000FF);

        //Check received function code for error
        if(rFc == 0x80 + fc || rFc == 0x83)
            throw(new ModbusResponseException(rMsg.getByteBuffer().get()));

        //Read the value
        int rByteCount = (int)(rMsg.getByteBuffer().get() & 0x000000FF);

        if(rByteCount == 4){
            return rMsg.getByteBuffer().getFloat();
        } else {
            throw(new ModbusResponseException(3));
        }
    }

    /**
     * Write the provided float value in the register at the
     * provided address.
     * @param address   The register address
     * @param value     The desired value
     * @return          True if write operation was successful
     * @throws ModbusResponseException
     */
    public boolean writeRegister(int address, float value) throws ModbusResponseException {
        byte fc = 0x10;

        //Convert float in integer
        int iFloat = Float.floatToIntBits(value);

        //Generate request byte buffer
        ByteBuffer request = ByteBuffer.allocate(10).put(fc).putShort((short)address).putShort((short)2).put((byte)4).putInt(iFloat);

        PDU msg = new PDU(request);
        MBAP header = new MBAP((short)1, protocolID, (short)(msg.getByteBuffer().capacity()+1), RTUAddr);

        Frame toSend = new Frame(header, msg);

        //Set request to the server
        Frame f = Transaction.process(toSend, in, out);

        if(f == null)   //Error receiving response
            throw(new NullPointerException());

        //Decode response
        PDU rMsg = f.getMessage();
        MBAP rHeader = f.getHeader();

        int rFc = (int)(rMsg.getByteBuffer().get() & 0x000000FF);
        int rAddr = (int)(rMsg.getByteBuffer().getShort() & 0x0000FFFF);

        //Check received function code for error
        if(rFc == 0x80 + fc || rFc == 0x90)
            throw(new ModbusResponseException(rMsg.getByteBuffer().get()));

        //Read the value
        int rByteCount = (int)(rMsg.getByteBuffer().getShort() & 0x0000FFFF);

        //Check if write was successful
        if(rByteCount == 2)
            return true;
        else
            return false;
    }

    /**
     * Main. For test purpose only.
     */
    public static void main(String[] args) {
        ModbusAccessor ma;
        boolean value = false;

        try {
            ma = ModbusAccessor.getInstance();

            try{
                System.out.println("Writing coil...");

                System.out.println("Response is " + String.valueOf(ma.writeCoil(401, false)));

                System.out.println("Done !\n");
                System.out.println("Reading coil...");

                value = ma.readCoil(401);

                System.out.println("Its value is : " + String.valueOf(value));
                System.out.println();

                System.out.println("Writing holding register...");

                System.out.println("Response is " + String.valueOf(ma.writeRegister(205, 0.5f)));

                System.out.println("Done !\n");

                System.out.println("Reading holding register...");

                System.out.println("Its value is : " + String.valueOf(ma.readRegister(205)));

                System.out.println("Writing holding register...");

                System.out.println("Response is " + String.valueOf(ma.writeRegister(205, 0.0f)));

                System.out.println("Done !\n");

                System.out.println("Reading holding register...");

                System.out.println("Its value is : " + String.valueOf(ma.readRegister(205)));
            } catch(NullPointerException | ModbusResponseException e) {
                e.printStackTrace();
                System.exit(1);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
