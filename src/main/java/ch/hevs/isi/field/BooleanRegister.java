package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;

import java.io.IOException;

public class BooleanRegister extends Register{

    private BinaryDataPoint bdp;

    public BooleanRegister(int address, String label, boolean isOutput, int range, int offset) {
        super(label, address, range, offset);

        bdp = new BinaryDataPoint(label, isOutput);
    }

    @Override
    public void read() throws IOException, ModbusResponseException {
        ModbusAccessor ma = ModbusAccessor.getInstance();

        bdp.setValue(ma.readCoil(getAddress()));
    }

    @Override
    public DataPoint getDP() {
        return bdp;
    }

    @Override
    public void write() throws IOException, ModbusResponseException {
        ModbusAccessor ma = ModbusAccessor.getInstance();

        if(bdp.isOutput()){
            ma.writeCoil(getAddress(), bdp.getValue());
        }
    }
}
