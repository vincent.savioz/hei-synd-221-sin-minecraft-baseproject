package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;

import java.io.IOException;

/**
 * This abstract class represent a Modbus register, which contains
 * every useful information in order to create a register.
 */
public abstract class Register {

    private int address, range, offset;

    /**
     * Constructor.
     * @param label     The label of the register
     * @param address   Its Modbus address
     * @param range     Its range value
     * @param offset    Its offset value
     */
    public Register(String label, int address, int range, int offset){
        this.address = address;
        this.range = range;
        this.offset = offset;
    }

    /**
     * Send the write command to the field
     * @throws IOException
     * @throws ModbusResponseException
     */
    public abstract void write() throws IOException, ModbusResponseException;

    /**
     * Send the read command to the field
     * @throws IOException
     * @throws ModbusResponseException
     */
    public abstract void read() throws IOException, ModbusResponseException;

    /**
     * Get the register datapoint
     * @return  The datapoint
     */
    public abstract DataPoint getDP();

    /**
     * Get the address
     * @return  The address
     */
    public int getAddress(){
        return address;
    }

    /**
     * Get the range value
     * @return  The range value
     */
    public int getRange(){
        return range;
    }

    /**
     * Get the offset value
     * @return  The offset value
     */
    public int getOffset(){
        return offset;
    }
}
