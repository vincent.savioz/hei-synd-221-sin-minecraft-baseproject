package ch.hevs.isi.field;

import java.util.TimerTask;

/**
 * This class is created on a timer timeout and send
 * the command to the field connector to read all
 * registers.
 * When it is done, the thread dies.
 * @version 1.0
 * @author Vincent
 */
public class PollTask extends TimerTask {

    /**
     * Constructor. Call the constructor of the superclass.
     */
    PollTask(){
        super();
    }

    /**
     * Is called by the timer to read all registers.
     */
    @Override
    public void run() {
        FieldConnector fc = FieldConnector.getInstance();
        fc.poll();
    }
}
