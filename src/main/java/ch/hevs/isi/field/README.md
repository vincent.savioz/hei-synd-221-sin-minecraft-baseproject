# Minecraft Electrical Age - Field package

This package manage all the field aspect of the project. The main parts are the *FieldConnector*
and *ModbusAccessor* classes. Other classes are used by these two.

## Useful information

The Modbus client (detailed in *ModbusAccessor*) read or write values at specified Modbus address,
analyze server response and look for errors. Depending on the desired action (read/write coil or
holding register), it automatically generates a proper MBAP (Modbus Header) and PDU (Modbus Message)
and transmit it to the server.

The Field Connector simply uses this *ModbusAccessor* class in order to communicate with the Minecraft
world. It was designed to be independant of the transmission protocol used. Field Connector also
use registers which are generated on initialization, using a CSV file containing all datapoints information.

## Authors and contributors

* **Vincent Savioz**
