package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * This abstract class is used to transmit frames
 * onto the output stream and read the input stream.
 * @version 2.0
 * @author Vincent
 */
public abstract class Transaction {

    /**
     * Process the transaction, which consist of sending the frame
     * onto the provided output stream and read the answer in the
     * input stream.
     * @param f     The frame to send
     * @param in    The input stream
     * @param out   The output stream
     * @return      The received frame
     */
    public static Frame process(Frame f, InputStream in, OutputStream out){
        byte[] read;

        try{
            //Send the frame ByteBuffer in the output stream
            Utility.sendBytes(out, f.getByteBuffer().array(), 0, f.getByteBuffer().capacity());

            //Wait for the answer
            while((read = Utility.readBytes(in)) == null);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return new Frame(ByteBuffer.wrap(read));
    }

}
