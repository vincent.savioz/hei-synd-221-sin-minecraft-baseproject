package ch.hevs.isi.field;

import java.nio.ByteBuffer;

/**
 * This class represents a Modbus frame, containing a header
 * (MBAP) and a message (PDU).
 * @version 2.3
 * @author Vincent
 */
public class Frame {

    private MBAP header;
    private PDU message;
    private ByteBuffer bb;

    /**
     * Constructor using a MBAP and PDU objects
     * @param m     The MBAP
     * @param p     The PDU
     */
    public Frame(MBAP m, PDU p){
        header = m;
        message = p;

        //Reset both position to 0
        m.getByteBuffer().position(0);
        p.getByteBuffer().position(0);

        //Merge the two ByteBuffers
        bb = ByteBuffer.allocate(m.getByteBuffer().limit() + p.getByteBuffer().limit());

        byte[] mbap = m.getByteBuffer().array();
        byte[] pdu = p.getByteBuffer().array();

        //Put MPAB
        for(int i = 0; i < mbap.length; i++)
            bb.put(mbap[i]);

        for(int i = 0; i < pdu.length; i++)
            bb.put(pdu[i]);

        //Reset position
        bb.position(0);
    }

    /**
     * Constructor using a ByteBuffer
     * @param b     The ByteBuffer
     */
    public Frame(ByteBuffer b){
        bb = b;

        //Generate the PDU and MBAP from the ByteBuffer
        byte[] bHeader = new byte[7];
        byte[] bMsg = new byte[b.capacity()-7];

        b.get(bHeader);
        b.get(bMsg);

        header = new MBAP(ByteBuffer.wrap(bHeader));
        message = new PDU(ByteBuffer.wrap(bMsg));
    }

    /**
     * Get the header (MBAP)
     * @return  The header
     */
    public MBAP getHeader(){
        return header;
    }

    /**
     * Get the message (PDU)
     * @return
     */
    public PDU getMessage(){
        return message;
    }

    /**
     * Get the ByteBuffer
     * @return  The ByteBuffer
     */
    public ByteBuffer getByteBuffer(){
        return bb;
    }
}
