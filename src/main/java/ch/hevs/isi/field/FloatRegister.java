package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;
import java.sql.SQLOutput;

/**
 * This class implement a Modbus float register,
 * which is in reality two 16-bit registers.
 * @version 1.1
 * @author Vincent
 */
public class FloatRegister extends Register {
    private FloatDataPoint fdp;

    /**
     * Constructor. Create a float register
     * @param address   The Modbus address of the first register
     * @param label     The corresponding label
     * @param isOutput  Is the register an output?
     * @param range     The range value
     * @param offset    The offset value
     */
    public FloatRegister(int address, String label, boolean isOutput, int range, int offset) {
        super(label, address, range, offset);

        //Create a new float datapoint
        fdp = new FloatDataPoint(label, isOutput);
    }

    /**
     * Implements the method from the Register class
     * @throws IOException
     * @throws ModbusResponseException
     */
    @Override
    public void read() throws IOException, ModbusResponseException {
        ModbusAccessor ma = ModbusAccessor.getInstance();
        float value = ma.readRegister(getAddress());

        fdp.setValue(value * getRange() + getOffset());
    }

    @Override
    public DataPoint getDP() {
        return fdp;
    }

    /**
     * Implements the method from the Register class
     * @throws IOException
     * @throws ModbusResponseException
     */
    @Override
    public void write() throws IOException, ModbusResponseException {
        ModbusAccessor ma = ModbusAccessor.getInstance();

        //Write only if datapoint is an output
        if(fdp.isOutput()){
            ma.writeRegister(getAddress(), (fdp.getValue() - getOffset() / getRange()));
        }
    }
}
