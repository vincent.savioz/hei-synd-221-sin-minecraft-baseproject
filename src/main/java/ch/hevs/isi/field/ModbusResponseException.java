package ch.hevs.isi.field;

/**
 * This class materialize a Modbus communication error.
 * Base on the provided error code, it returns the error
 * type.
 * @version 1.0
 * @author Vincent
 */
public class ModbusResponseException extends Throwable {

    String err;

    /**
     * Save the error message in a string.
     * @param errorCode     The error code
     */
    public ModbusResponseException(int errorCode){
        err = errorCode(errorCode);
    }

    /**
     * Override the toString method from the Object class
     * @return      The string representation of the error
     */
    public String toString(){
        return err;
    }

    /**
     * Provide the right error message based on the provided error code.
     * @param errorType     The error code
     * @return              The error message
     */
    private String errorCode(int errorType){
        switch(errorType){
            case 1:
                return "ILLEGAL FUNCTION";

            case 2:
                return "ILLEGAL DATA ADDRESS ";

            case 3:
                return "ILLEGAL DATA VALUE ";

            case 4:
                return "SLAVE DEVICE FAILURE ";
            case 5:
                return "ACKNOWLEDGE";

            case 6:
                return "SLAVE DEVICE BUSY";

            case 8:
                return "MEMORY PARITY ERROR";

            case 10:
                return "GATEWAY PATH UNAVAILABLE";

            case 11:
                return "GATEWAY TARGET DEVICE, FAILED TO RESPOND";

            default:
                return "UNKNOWN ERROR";
        }
    }

}
