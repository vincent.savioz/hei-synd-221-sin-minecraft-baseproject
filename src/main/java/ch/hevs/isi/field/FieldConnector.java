package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;

import java.io.*;
import java.util.HashMap;
import java.util.Timer;

/**
 * This class connects the field (which is the modbus protocol) to the inner
 * part of the program. The main advantage of that method is that the field
 * connector is unaware of the underlying protocol nor the real field (Minecraft).
 * This class uses a singleton pattern.
 * @version 1.2
 * @author Vincent
 */
public class FieldConnector implements DataPointListener {

    private static final int pollTime = 3000; //Poll inputs every 5s

    private static HashMap<String, Register> registerMap = new HashMap<>();
    private static FieldConnector fc = null;
    private static Timer pollTimer;

    /**
     * Private constructor.
     */
    private FieldConnector(){

    }

    /**
     * Get the only instance of the class
     * @return      The instance
     */
    public static FieldConnector getInstance(){
        if(fc == null){     //Initialize on first method call
            fc = new FieldConnector();

            //Read the registers from CSV file
            loadRegister("data\\ModbusMap.csv");

            //Start timer
            pollTimer = new Timer();
            pollTimer.scheduleAtFixedRate(new PollTask(), 0, pollTime);
        }

        return fc;
    }

    /**
     * Send a write command to the field using the label as
     * reference.
     * @param label     The label of the datapoint/register
     * @param value     Its value (not used, since we get the value from the datapoint)
     */
    private void pushToField(String label, String value){
        //Retrieve the register corresponding to the label
        Register r = getRegisterFromLabel(label);

        try{
            //Send write command to the field
            r.write();

            System.out.println("Writing the register " + label + " at value " + value);
        } catch(ModbusResponseException | IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Override method from the DatapointListener interface
     * @param fdp    The modified float datapoint
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        String label = fdp.getLabel();
        float value = fdp.getValue();

        //Send information to the field
        pushToField(label, String.valueOf(value));
    }

    /**
     * Override method from the DatapointListener interface
     * @param bdp    The modified binary datapoint
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        String label = bdp.getLabel();
        boolean value = bdp.getValue();

        //Send information to the field
        pushToField(label, String.valueOf(value));
    }

    /**
     * Read every register when the method is called (from the timer).
     */
    public void poll(){
        try {
            System.out.println("poll() ...");
            //Read all registers
            for (Register r : registerMap.values()) {
                r.read();
            }
        } catch(IOException | ModbusResponseException e){
            e.printStackTrace();
        }
    }

    /**
     * Get the register corresponding to the provided label.
     * @param label     The register label
     * @return          The corresponding register
     */
    public static Register getRegisterFromLabel(String label){
        return registerMap.get(label);
    }

    /**
     * Load register from the CSV file (values separated by ';')
     * @param path      The path to the file
     */
    private static void loadRegister(String path){
        //Matching column index
        final int labelC = 0, typeC = 1, inputC = 2, outputC = 3, addressC = 4, rangeC = 5, offsetC = 6;
        final String splitSymbol = ";";

        try {
            String label;
            char type;
            boolean isOutput;
            int address, range, offset;

            String line;
            String[] splited;
            boolean firstLine = true;

            //Create a new reader
            BufferedReader br = Utility.fileParser(null, "ModbusMap.csv");

            //Load register as until end of file
            while ((line = br.readLine()) != null) {
                if(firstLine){  //Skip if first line
                    firstLine = false;
                    continue;
                }

                //Split line in an array of string
                splited = line.split(splitSymbol);

                //Retrieve label and type
                label = splited[labelC];
                type = splited[typeC].charAt(0);

                //Is the register an output or an input?
                if(splited[outputC].equals("Y"))
                    isOutput = true;
                else
                    isOutput = false;

                //Read Modbus address, its range and offset values
                address = Integer.valueOf(splited[addressC]);
                range = Integer.valueOf(splited[rangeC]);
                offset = Integer.valueOf(splited[offsetC]);

                //Create corresponding register
                if(type == 'F')
                    registerMap.put(label, new FloatRegister(address, label, isOutput, range, offset));
                else
                    registerMap.put(label, new BooleanRegister(address, label, isOutput, range, offset));

                System.out.println("Loading register " + label);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Done !\n");
    }

    public static void main(String[] args) {
        FieldConnector fc = FieldConnector.getInstance();
    }
}
