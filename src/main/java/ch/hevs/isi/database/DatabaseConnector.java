package ch.hevs.isi.database;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

public class DatabaseConnector implements DataPointListener {

    private static DatabaseConnector dc = null;

    /**
     * Constructor of the DatabaseConnector
     */
    private DatabaseConnector(){}

    /**
     * Create a single instance of DatabaseConnector
     *
     * @return a DatabaseConnector
     */
    public static DatabaseConnector getInstance(){
        if(dc == null){
            dc = new DatabaseConnector();
        }

        return dc;
    }

    /**
     * Method used to push a Value to the right Label in the Database of influx
     * by using HTTP POST
     * Starts by initializing the connection with the database and write the body
     * of the HTTP request
     *
     * @param label : Label of the Datapoint
     * @param value : Value of the Datapoint
     * @throws IOException
     */
    private void pushToDatabase(String label, String value) throws IOException {
        //System.out.println("pushToDatabase : label : " + label + " value : " + value);

        //Construct a URL Object
        URL url = new URL("https://influx.sdi.hevs.ch/write?db=SIn35");
        //Invoke the URLto retrieve an HttpURLConnection
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        //Configure the HttpURLConnection
        String userpass = "SIn35:" + Utility.md5sum("SIn35");
        String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
        connection.setRequestProperty ("Authorization", "Basic " + encoding);
        connection.setRequestProperty("Content-Type", "binary/octet-stream");

        //Set the HTTP Method to post and indicate that there is a message body
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);

        //Fetch an OutputStreamWriter object for the HttpURLConnection
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        //Fetch a BufferedReader object for the HttpURLConnection


        //Write the body of the HTTP request
        writer.write( label + " value=" + value);
        writer.flush();

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        int responseCode = connection.getResponseCode();
        //System.out.println("Response Code : " + responseCode);

        while ((in.readLine()) != null) {

        };
        connection.disconnect();
    }

    /**
     * Method used to push to the Database the new value of a FloatDataPoint
     * OverWritten Method from the interface DataPointListener
     *
     * @param fdp : FloatDataPoint
     * @throws IOException
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) throws IOException {
        String label = fdp.getLabel();
        float value = fdp.getValue();

        pushToDatabase(label, String.valueOf(value));
    }

    /**
     * Method used to push to the Database the new value of a BinaryDataPoint
     * OverWritten Method from the interface DataPointListener
     *
     * @param bdp : BinaryDataPoint
     * @throws IOException
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) throws IOException {
        String label = bdp.getLabel();

        if(bdp.getValue()){
            pushToDatabase(label, "1");
        }else{
            pushToDatabase(label, "0");
        }
    }
}
