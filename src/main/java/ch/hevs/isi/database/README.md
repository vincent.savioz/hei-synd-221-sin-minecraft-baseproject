# Minecraft Electrical Age - Core package

This package, which contains the *DatabaseConnector* class, is used to push datapoints
values to the InfluxDB database.

## Useful information

When a datapoint's value has changed, *DatabaseConnector* take this value and the
datapoint's label and push it to the database using a HTTP Post. It first establish 
the connection between the program and InfluxDB.

## Authors and contributors

* **Filip Simeonovic**
