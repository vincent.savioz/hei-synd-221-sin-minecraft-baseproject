package ch.hevs.isi.web;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.database.DatabaseConnector;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Vector;

public class WebConnector extends WebSocketServer implements DataPointListener{

    private static WebConnector wc = null;
    //Create a list of WebSocket to store all the elements
    private Vector<WebSocket> socketList = new Vector<WebSocket>();

    /**
     * Constructor used to set the port for the communication
     */
    private WebConnector(){
        super(new InetSocketAddress(8888));
        start();
    }

    /**
     * Method used to add a WebSocket to our list when it connects
     * OverWritten Method from the Class WebSocketServer
     *
     * @param webSocket : a WebSocket
     * @param clientHandshake : handshake allows the server to authenticate itself to the
     * client by using public-key techniques
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        socketList.add(webSocket);
        System.out.println("onOpen");

    }

    /**
     * Method used when the web interface is closed
     * The program does not use this method
     * OverWritten Method from the Class WebSocketServer
     *
     *
     * @param webSocket
     * @param i
     * @param s
     * @param b
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        System.out.println("onClose");
        System.out.println(s);
    }

    /**
     * Method used to get the right Label according to the message
     * by using getDataPointFromLabel
     * OverWritten Method from the Class WebSocketServer
     *
     * @param webSocket : A WebSocket
     * @param s : Message received
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        try {
        System.out.println("onMessage");
        System.out.println(s);

        String[] parts = s.split("=");

        System.out.println(parts[0]);
        System.out.println(parts[1]);

        DataPoint.getDataPointFromLabel(parts[0]).fromString(parts[1]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method used when there is an error with the interface
     * The program does not use this method
     * OverWritten Method from the Class WebSocketServer
     *
     * @param webSocket : a WebSocket
     * @param e : an exception
     */
    @Override
    public void onError(WebSocket webSocket, Exception e) {
        System.out.println("onError");
    }

    /**
     * Method used when the interface start
     * The program does not use this method
     * OverWritten Method from the Class WebSocketServer
     */
    @Override
    public void onStart() {
        System.out.println("onStart");
    }

    /**
     * Create a single instance of WebConnector
     *
     * @return a WebConnector
     */
    public static WebConnector getInstance(){
        if(wc == null){
            wc = new WebConnector();
        }

        return wc;
    }

    /**
     * Method used to send to every WebPages the new values of each labels
     * by using the method broadcast
     * Broadcast comes from the class WebSocketServer
     *
     * @param label : Label of the Datapoint
     * @param value : Value of the Datapoint
     */
    private void pushToWebPages(String label, String value){
        broadcast(label + "=" + value);

        //System.out.println("pushWebPages : label : " + label + " value : " + value);
    }

    /**
     * Method used to push to the WebPages the new value of a FloatDataPoint
     * OverWritten Method from the interface DataPointListener
     *
     * @param fdp : FloatDataPoint
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        String label = fdp.getLabel();
        float value = fdp.getValue();

        pushToWebPages(label, String.valueOf(value));
    }

    /**
     * Method used to push to the WebPages the new value of a BinaryDataPoint
     * OverWritten Method from the interface DataPointListener
     *
     * @param bdp : BinaryDataPoint
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        String label = bdp.getLabel();

        if(bdp.getValue()){
            pushToWebPages(label, "1");
        }else{
            pushToWebPages(label, "0");
        }
    }
}
