# Minecraft Electrical Age - Web package

This package, containing only the *WebConnector* class, is the interface between the
program (datapoints) and a webpage.

## Useful information

When a datapoint value has changed, the class is informed and update value on the webpage.
Conversely, when a value has changed on the webpage, it automatically change the value of the
concerned datapoint.

Note: The port used for the communication between the program and the webpage is 8888.

## Authors and contributors

* **Filip Simeonovic**
