# Minecraft Electrical Age

This project aims at controlling a complete Minecraft electrical grid from a Java program. You can either control the world via a dedicated webpage, 
directly from the Minecraft world or using the program inner controller. Data is logged in an InfluxDB database.

## Getting Started

This project is made to run on IntelliJ. Directely fork this repository and start developping ! The "How To for installing Java Developpment 
Tools and Minecraft" can be found  [here](https://cyberlearn.hes-so.ch/pluginfile.php/3567904/mod_label/intro/EA-ToolsToInstall.pdf?time=1618320009733).

Note: This project must run on [**Java JDK 8**](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) !


### Installing

First, you will need to download Minecraft Electrical Age directly from [this repository](https://github.com/patrudaz/ElectricalAge/archive/refs/heads/develop.zip) 
and extract it in your preferred directory. Then, open a command prompt and install the dev workspace:

```
gradlew setupDevWorkspace
```

When the workspace is properly set up, you need to run the following command in order to launch Minecraft:

```
gradlew runClient
```

After Minecraft is properly launched, close it and download [the OpenComputer mod](http://maven.cil.li/li/cil/oc/OpenComputers/MC1.7.10-1.7.3.1258/OpenComputers-MC1.7.10-1.7.3.1258-dev.jar). 
Install it in the */run/mod* folder of your newly created workspace.

Before being completely able to run the project, one little thing needs to be done. Open the */run/config/Eln.cfg* file and modify the 101, 107 and 108 lines:

```
modbus {
    B:enable=true	    //<----
    I:port=1502
}


simulation {
    I:electricalFrequency=80		              //<----
    I:electricalInterSystemOverSampling=20		//<----
    I:thermalFrequency=400
}
```

Minecraft default world can be found [here](https://cyberlearn.hes-so.ch/pluginfile.php/3567904/mod_label/intro/SIn_2021_0.1.0.zip) and must be unzipped in the */run/saves* folder.

## Useful information

The Minecraft Electical Age Modbus server runs on the address **localhost:1502**.

You will also need a few parameters in order to properly launch the program:

```
<InfluxDB Server> <Group Name> <ModbusTCP Server> <modbus TCP port> [-modbus4j]
```

Javadoc has been completely generated and is located under the */javadoc/* project subfolder.

## Authors and contributors

* **Filip Simeonovic** - *Core, Database and Webpage developper*
* **Vincent Savioz** - *Smart Controller and Field (Modbus) developper*
* **Patrice Rudaz** - *Base project developper*
* **Dominique Gabioud** - *Base project developper, professor*
